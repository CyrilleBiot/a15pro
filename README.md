# a15pro

Juste un essai de paquet en amd64 du driver pour a15pro
[Source (https://veikk.com/upload/file/20211022/vktablet-1.0.0-1.x86_64.rpm)]

Conversion du rpm au format deb


## Installation

`# dpkg -i a15pro_1.0_all.deb`


## Suppression

`# dpkg -r a15pro`

## Devel / Unstable

N'ayant pas de matériel , non testé

Pas de gestion des dépedances dans le fichier install
