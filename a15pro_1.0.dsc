Format: 1.0
Source: a15pro
Binary: a15pro
Architecture: all
Version: 1.0
Maintainer: Cyrille Biot <cyrille@cbiot.fr>
Homepage: https://cbiot.fr
Build-Depends: debhelper (>= 9)
Package-List:
 a15pro deb system extra arch=all
Checksums-Sha1:
 c27c7ec1444e34d31ea3326538134051ed9c88f7 22598512 a15pro_1.0.tar.gz
Checksums-Sha256:
 3c3c6cd05d8eb66ca2da299d5dba2a3c8ba0f5925f05497e873afc6780c1141e 22598512 a15pro_1.0.tar.gz
Files:
 573410013e8b0d0653edbb57b5f1271e 22598512 a15pro_1.0.tar.gz
